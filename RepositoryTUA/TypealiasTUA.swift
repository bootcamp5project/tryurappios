//
//  TypealiasTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public typealias ErrorClosure = (Error) -> Void
public typealias UserSuccessClosure = (UserTUA) -> Void
public typealias ResponseSuccessClosure = (ResponseTUA) -> Void
public typealias ResponseUsersSuccessClosure = (ResponseUsersTUA) -> Void
public typealias ResponseBusinessSuccessClosure = (ResponseBusinessTUA) -> Void
public typealias ResponseBusinessesSuccessClosure = (ResponseBusinessesTUA) -> Void
