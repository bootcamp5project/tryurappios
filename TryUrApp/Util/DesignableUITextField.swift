//
//  DesignableUITextField.swift
//  TryUrApp
//
//  Created by Henry Bravo on 5/23/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            imageView.image = image
            
            var width = leftPadding + 20
            
            if (borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line) {
                width = width + 10
            }
            
            imageView.tintColor = color
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
            // Note: In order for your image to use the tint color, you have to select
            // the image in the Assets.xcassets and change the "Render As" property to
            // "Template Image".
            
            leftView = view
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "",
                                                   attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}

extension DesignableUITextField {
    func setBottomBorder() {
        self.autocorrectionType = UITextAutocorrectionType.no
        
        if let placeholderText = self.placeholder {
            
            let centeredParagraphStyle = NSMutableParagraphStyle()
            centeredParagraphStyle.alignment = .natural
            let attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.paragraphStyle: centeredParagraphStyle])
            self.attributedPlaceholder = attributedPlaceholder
        }
        
        self.layer.shadowColor = UIColor(hexString: "#C7C7CC").cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        
    }
}
