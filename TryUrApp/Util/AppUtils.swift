//
//  AppUtils.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/6/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class AppUtils {
    
    // Verify if an email address is valid
    class func emailValidator(email: String) -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\\“(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\\“)@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    // Verify if a password is valid
    class func passwordValidator(password: String) -> Bool {
        let emailRegEx = "(?:(?:(?=.*?[0-9])(?=.*?[-!@#$%&*ˆ+=_])|(?:(?=.*?[0-9])|(?=.*?[A-Z])|(?=.*?[-!@#$%&*ˆ+=_])))|" +
            "(?=.*?[a-z])(?=.*?[0-9])(?=.*?[-!@#$%&*ˆ+=_]))[A-Za-z0-9-!@#$%&*ˆ+=_]{6,15}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: password)
    }
    
    // Verify if we got token saved
    class func verifyTokenExist() -> Bool {
        if let _ = KeychainWrapper.standard.string(forKey: "TOKEN") {
            return true
        } else { return false }
    }
    
    // Verify if is a valid url
    class func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    // Return the logged user
    class func getUserFromLocal() -> UserUI? {
        guard let userEmail = KeychainWrapper.standard.string(forKey: "USEREMAIL"),
              let userPassword = KeychainWrapper.standard.string(forKey: "PASSWORD")
              else { return nil }
        
        return UserUI(email: userEmail, name: "", password: userPassword)
    }
    
    class func getDate(from dateString: String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        dateFormatter.locale = Locale.init(identifier: "en_US")
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
        let date = dateFormatter.string(from: dateObj!)
        
        return date
    }
    
    class func clearKeyChain() {
        if AppUtils.verifyTokenExist() {
            KeychainWrapper.standard.removeObject(forKey: "TOKEN")
            KeychainWrapper.standard.removeObject(forKey: "USEREMAIL")
//            self.presentMessage("User logged out successfully.")
        } else {
//            let error: Error = MyError.customError(message: "There is no user logged in")
//            presentError(error)
        }
    }
}
