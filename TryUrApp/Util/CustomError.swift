//
//  CustomError.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/18/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public enum MyError: Error {
    case customError(message: String)
}

extension MyError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .customError(message: let message):
            return NSLocalizedString(message, comment: "MyError")
        }
    }
}
