//
//  Constants.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/22/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

struct Constants {
    // Background color
    static let backgroundColorHex = UIColor(hexString: "545B61", alpha: 1)
    
    // UITextFields text color
    static let uitextfieldTextColorHex = UIColor(hexString: "C7C7CC", alpha: 1)
}
