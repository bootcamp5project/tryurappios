//
//  UITextField+BottomBorderAndPadding.swift
//  TryUrApp
//
//  Created by Henry Bravo on 5/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

extension UITextField {
    func setPadding() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
