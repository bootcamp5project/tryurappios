//
//  UIViewController+PresentMessage.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/18/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentError(_ error: Error) {
        let alertController = UIAlertController(title: "Error",
                                                message: error.localizedDescription,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default))
        self.present(alertController, animated: true)
    }
    
    func presentMessage(_ message: String) {
        let alertController = UIAlertController(title: "Message",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default))
        self.present(alertController, animated: true)
    }
    
    func closeViewController(with message: String) {
        let alertController = UIAlertController(title: "Add business",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default, handler: { action in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true)
    }
}
