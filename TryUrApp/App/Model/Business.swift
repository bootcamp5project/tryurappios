import Foundation

class Business {
    
   var userEmail : String = ""
   var url: String = ""
   var businessUser : String = ""
   var businessName: String = ""
   var idBusinessType: Int = -1
   var idCmsType: Int = -1
   var status = "new"
    
    public init(businessName: String) {
                self.businessName = businessName
    }
    
    
    public init() {}
}
