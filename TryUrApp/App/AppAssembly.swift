//
//  AppAssembly.swift
//  TryUrApp
//
//  Created by Henry Bravo on 5/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

final class AppAssembly {
    private(set) lazy var window = UIWindow(frame: UIScreen.main.bounds)
    lazy var navController = UINavigationController()
    private(set) lazy var coreAssembly = CoreAssembly()
}
