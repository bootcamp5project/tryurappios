//
//  IsNewBusiness.swift
//  TryUrApp
//
//  Created by Pedro Sánchez Castro on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class IsNewBusiness: UIViewController {
    
   
    @IBOutlet weak var businessName: UILabel!
    var businessDisplay: Business?
    
    override func viewWillAppear(_ animated: Bool) {
        businessDisplay = (self.parent as! BusinessDetailViewController).businessDisplay
        super.viewDidLoad()
        
        businessName.text = businessDisplay?.businessName
    }
    
    override func viewDidLoad() {
        businessDisplay = (self.parent as! BusinessDetailViewController).businessDisplay
        super.viewDidLoad()
        
        businessName.text = businessDisplay?.businessName
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
