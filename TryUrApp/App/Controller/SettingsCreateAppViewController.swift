import UIKit

class SettingsCreateAppViewController: UIViewController,  UIPickerViewDelegate, UIPickerViewDataSource {

    let pickerData = [String](arrayLiteral: "1 Colum", "2 Colum")
    
    @IBOutlet weak var type: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Init
        type.text = "1 Column"
        
       
        //delegate
        let thePicker = UIPickerView()
        
        thePicker.delegate = self
        
        type.inputView = thePicker

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Picker View Delegated
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        type.text = pickerData[row]
        self.view.endEditing(true)
    }

}
