

import UIKit

final class BusinessDetailViewController: UIViewController {
    
   
    // Add new Business
    @IBAction func addNewBusiness(_ sender: Any) {
        self.performSegue(withIdentifier: "addNewBusiness", sender: nil)
    }
    
   
    
    
    // Negocio a mostrar
    var businessDisplay: Business?
    
    // Cargamos los controladores segun el estado del negocio cargamos una vista u otra
    private lazy var noBusiness: NoBusinessViewController = { loadViewControllers(identifier: "NoBusinessViewController") as! NoBusinessViewController } ()
    private lazy var isNewBusiness: IsNewBusiness = { loadViewControllers(identifier: "IsNewBusiness") as! IsNewBusiness }()
    private lazy var appMakerDone: AppMakerDoneViewController = { loadViewControllers(identifier:  "AppMakerDoneViewController") as! AppMakerDoneViewController }()
    private lazy var appPurchased: AppPurchasedViewController = { loadViewControllers(identifier: "AppPurchasedViewController") as! AppPurchasedViewController}()
    
    
    @IBOutlet weak var bussinesName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: IBActions
    @IBAction func saveBusiness(_ segue: UIStoryboardSegue) {
        
        // Read from property in AddBusinessViewController
        guard let addBusinessController = segue.source as? AddBusinessViewController,
        let business = addBusinessController.business else {return}
        
        // Save business. Call to server
        MockData.allBusiness.append(business)
        
        //Update business after selection
        businessDisplay = business
        
    }
    
    @IBAction func changeBusiness(_ segue: UIStoryboardSegue)
    {
       // Read from property
       guard let businessCollectionVC = segue.source as? BusinessCollectionViewController,
       let indexpath = businessCollectionVC.businessIndex else {return}
        
      // Update
       businessDisplay = MockData.allBusiness[indexpath]
       //bussinesName.text = businessDisplay?.businessName
 
    }
    
    @IBAction func cancelBusiness(_ Segue: UIStoryboardSegue)
    {
        
    }
    
    @IBAction func updateBusiness(_ Segue: UIStoryboardSegue){
        //Update business status
        nextStatus()
        
        //Update
        setUpView()
    }
    
    
    // Cargamos una vista u otra dependiendo del estado del negocio
    func setUpView() {
//        if (MockData.allBusiness.capacity == 0) {
//            // Cargo is new
//            add(asChildViewController: noBusiness )
//
//        } else if (businessDisplay?.status == "new") {
//            // Cargo app generation done
//            remove(asChildViewController: noBusiness)
//            add(asChildViewController: isNewBusiness )
//
//        } else if (businessDisplay?.status == "done") {
//            // Cargo app generation done
//            remove(asChildViewController: isNewBusiness)
//            add(asChildViewController: appMakerDone )
//
//        } else if (businessDisplay?.status == "purchased") {
//            // Cargo app generation done
//            remove(asChildViewController: appMakerDone)
//            add(asChildViewController: appPurchased )
//        }
        
//        let myBusinessTUA: BusinessTUA = mapBusiness(from: aBusiness)
//        let data = NSKeyedArchiver.archivedData(withRootObject: myBusinessTUA)
//        UserDefaults.standard.set(data, forKey: "BUSINESS")
        
//        if let data = NSUserDefaults.standardUserDefaults().objectForKey("books") as? NSData {
//            let books = NSKeyedUnarchiver.unarchiveObjectWithData(data)
//        }
        
        if let data = UserDefaults.standard.object(forKey: "BUSINESS") as? Data {
            if let business = NSKeyedUnarchiver.unarchiveObject(with: data) as? BusinessTUA {
                print("BUSINESS \(business.url ?? "No BUSINESS.URL")")
                // add(asChildViewController: showBusiness )
                // self.performSegue(withIdentifier: "showBusiness", sender: nil)
            } else {
                print("No BUSINESS")
                add(asChildViewController: noBusiness )
            }
        } else {
            print("No DATA BUSINESS")
            add(asChildViewController: noBusiness )
        }
            
    }
    
    func nextStatus() {
        if (businessDisplay?.status=="new"){
            businessDisplay?.status="done"
        } else if (businessDisplay?.status=="done"){
            businessDisplay?.status="purchased"

        }
    }
    
}
