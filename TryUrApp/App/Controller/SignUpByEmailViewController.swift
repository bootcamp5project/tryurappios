//
//  SignUpByEmailViewController.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/17/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class SignUpByEmailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: DesignableUITextField!
    @IBOutlet weak var emailTextField: DesignableUITextField!
    @IBOutlet weak var passwordTextField: DesignableUITextField!
    @IBOutlet weak var retypePasswordTextField: DesignableUITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    let addUserInteractorImpl = AddUserInteractorImpl()
    let appAssembly = AppAssembly()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        let name = nameTextField.rx.text.map { $0 ?? "" }
        let email = emailTextField.rx.text.map { $0 ?? "" }
        let password = passwordTextField.rx.text.map { $0 ?? "" }
        let confirmPassword = retypePasswordTextField.rx.text.map { $0 ?? "" }
        
        let signUpByEmailViewModel = SignUpByEmailViewModel(with: name, email: email, password: password, confimPassword: confirmPassword)
        

        setupUI()
        configure(with: signUpByEmailViewModel)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpButtonTap(_ sender: Any) {
        let sV = UIViewController.displaySpinner(onView: self.view)
        
        let user = UserUI(email: emailTextField.text!, name: nameTextField.text!, password: passwordTextField.text!)
        addUserInteractorImpl.execute(user: user, success: { [weak self] (response) in
            UIViewController.removeSpinner(spinner: sV)
            self?.presentMessage("User registered successfully.")
            print(response.result ?? "Nothing")
        }) { [weak self] (error) in
            UIViewController.removeSpinner(spinner: sV)
            self?.presentError(error)
        }
    }
    
    @IBAction func alreadyRegisteredButtonTap(_ sender: Any) {
        let signInByEmailViewController = appAssembly.coreAssembly.signInByEmailViewController
        self.present(signInByEmailViewController, animated: true, completion: nil)
    }
    
    
}

extension SignUpByEmailViewController {
    
    func setupUI() {
        nameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        retypePasswordTextField.delegate = self
        
        nameTextField.setBottomBorder()
        nameTextField.clearButtonMode = .whileEditing
        emailTextField.setBottomBorder()
        emailTextField.clearButtonMode = .whileEditing
        passwordTextField.setBottomBorder()
        passwordTextField.isSecureTextEntry = true
        passwordTextField.clearButtonMode = .whileEditing
        retypePasswordTextField.setBottomBorder()
        retypePasswordTextField.isSecureTextEntry = true
        retypePasswordTextField.clearButtonMode = .whileEditing
        
    }
    
    func configure(with viewModel: SignUpByEmailViewModel) {
        _ = viewModel.isValid.bind(to: signUpButton.rx.isEnabled)
        
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y - 120, width:self.view.frame.size.width, height:self.view.frame.size.height);
            
        })
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y + 120, width:self.view.frame.size.width, height:self.view.frame.size.height);
            
        })
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) //This will hide the keyboard
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            nameTextField.resignFirstResponder()
            self.emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            retypePasswordTextField.becomeFirstResponder()
        default:
            retypePasswordTextField.resignFirstResponder()
        }
        
        return true
    }
    
}
