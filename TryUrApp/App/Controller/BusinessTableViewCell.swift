//
//  BusinessTableViewCell.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 21/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class BusinessTableViewCell: UITableViewCell {
    
    var myTableViewController: UITableViewController?
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Sample Item"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = UIColor.white
        return label
    }()
    
    let actionButton: UIButton = {
        let button = UIButton(type: UIButtonType.roundedRect)
        button.setTitle("Show App", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12.0)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        button.backgroundColor = UIColor.init(hexString: "333333")
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.init(hexString: "DDDDDD").cgColor
        button.layer.cornerRadius = 5.0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = UIColor.init(hexString: "595F65")
        addSubview(nameLabel)
        addSubview(actionButton)
        
        actionButton.addTarget(self.myTableViewController, action: #selector(BusinessTableViewController.navigateToReactApp), for: .touchUpInside)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]-8-[v1(80)]-8-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": nameLabel, "v1": actionButton]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": nameLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-12-[v0]-12-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": actionButton]))
    }
    
}
