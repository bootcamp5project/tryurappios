//
//  AddNewBusinessViewController.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/24/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

public class AddNewBusinessViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var urlTextField: DesignableUITextField!
    @IBOutlet weak var cmsTypeTextField: DesignableUITextField!
    @IBOutlet weak var ecommerceSwitch: UISwitch!
    @IBOutlet weak var blogSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    
    var disposeBag = DisposeBag()
    let myPicker = UIPickerView()
    var idCmsType = Int64()
    let createBusinessInteractor = CreateBusinessInteractorImpl()
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        let url = urlTextField.rx.text.map { $0 ?? "" }
        let cmsType = cmsTypeTextField.rx.text.map { $0 ?? "" }
        let ecommerce = ecommerceSwitch.rx.value
        let blog = blogSwitch.rx.value
        
        let addNewBusinessViewModel = AddNewBusinessViewModel(url: url, cmsType: cmsType, ecommerce: ecommerce, blog: blog)
        
        configure(with: addNewBusinessViewModel)
        setupUI()
        setupPicker()
    }
    
    @IBAction func saveButtonTap(_ sender: Any) {
        let sV = UIViewController.displaySpinner(onView: self.view)
        
        if let aUser = AppUtils.getUserFromLocal() {
            let businessType = ecommerceSwitch.isOn.verify(with: blogSwitch.isOn).id
            let aBusiness = BusinessUI(url: urlTextField.text!, userEmail: aUser.email, idBusinessType: Int64(businessType), idCmsType: idCmsType)
            createBusinessInteractor.execute(business: aBusiness, user: aUser, success: { [weak self] (response) in
                UIViewController.removeSpinner(spinner: sV)
                if (response.status >= 400) {
                    let error: Error = MyError.customError(message: response.resultDescription)
                    self?.presentError(error)
                } else {
                    self?.closeViewController(with: "Business created")
                }
            }) { [weak self] (error) in
                UIAlertController.removeSpinner(spinner: sV)
                self?.presentError(error)
            }
        }
    }
    
    @IBAction func cancelButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension AddNewBusinessViewController {
    
    func setupUI() {
        
        urlTextField.delegate = self
        cmsTypeTextField.delegate = self
        
        urlTextField.setBottomBorder()
        urlTextField.clearButtonMode = .whileEditing
        cmsTypeTextField.setBottomBorder()
    }
    
    func configure(with viewModel: AddNewBusinessViewModel) {
        _ = viewModel.isValid.bind(to: saveButton.rx.isEnabled)
    }
    
    func setupPicker() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(hexString: "50ADE7", alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        cmsTypeTextField.inputView = myPicker
        cmsTypeTextField.inputAccessoryView = toolBar
        
        Observable.just(cmsType.allCases)
            .bind(to: self.myPicker.rx.itemTitles) { _, item in
                return "\(item.description)"
            }
            .disposed(by: disposeBag)
        
        self.myPicker.rx.itemSelected
            .subscribe(onNext: { [weak self] row, component in
                print(row)
                self?.idCmsType = Int64(row)
                self?.cmsTypeTextField.text = cmsType.allCases[row].description
                //self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.font = UIFont (name: "Comfortaa Light", size: 14)
        label.textAlignment = .center
        return label
    }
    
    @objc func donePicker() {
        let row = myPicker.selectedRow(inComponent: 0)
        cmsTypeTextField.text = cmsType.allCases[row].description
        cmsTypeTextField.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        cmsTypeTextField.resignFirstResponder()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y - 90, width:self.view.frame.size.width, height:self.view.frame.size.height);
            
        })
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y + 90, width:self.view.frame.size.width, height:self.view.frame.size.height);
            
        })
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) //This will hide the keyboard
    }
}


enum cmsType: CaseIterable {
    case WP, MG, PS, JOO
    
    var description: String {
        switch self {
        case .WP: return "WordPress"
        case .MG: return "Magento"
        case .PS: return "PrestaShop"
        case .JOO: return "Joomla!"
        }
    }
}
