import UIKit


class LoginViewController: UIViewController{
    
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    
    @IBOutlet weak var loginbuttom: UIButton!
    /*
    @IBAction func loginAction(_ sender: UIButton) {
        
        // Spinner
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        guard let email = emailLabel.text else{
            return
        }
        
        guard let password = passwordLabel.text else {
            return
        }
        
        // Form Validation
        if(email=="" || password==""){
            errorLabel.text = "Insertar email y/o password"
            errorLabel.isHidden = false
            return
        }
        
        let user = UserUI(email: email, name:"", password: password)
        
        // Interactor --> 
        let authUserInteractor = AuthenticateUserInteractorImpl()
        authUserInteractor.execute(user: user, success: { (response) in
             UIViewController.removeSpinner(spinner: sv)
            if(response.status == 200){
            
            // Save user. Change for keyChain !!!
              UserDefaults.standard.set(response.resultDescription, forKey: "token")
              UserDefaults.standard.set(user.email, forKey: "USER")
                
            self.performSegue(withIdentifier: "loginOkSegue", sender: nil)
            } else {
                self.errorLabel.isHidden = false
                self.errorLabel.text = response.resultDescription
            }
            
        }) { (error) in
            self.errorLabel.text = error.localizedDescription
            self.errorLabel.isHidden = false
            
        }
    }
    */
    @IBAction func loginAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "loginOkSegue", sender: nil)
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hide label
        errorLabel.isHidden = true
        passwordLabel.isSecureTextEntry = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
    }

}

