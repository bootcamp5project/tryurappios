//
//  SignUpViewController.swift
//  TryUrApp
//
//  Created by Henry Bravo on 5/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

public class SignUpViewController: UIViewController {
    @IBOutlet weak var signUpWithEmailBtn: UIButton!
    
    let appAssembly = AppAssembly()
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    @IBAction func emailSignIn(_ sender: Any) {
        
        let emailSignUpViewController = SignUpByEmailViewController()
        self.present(emailSignUpViewController, animated: true, completion: nil)
    }
    
    @IBAction func alreadyRegisteredButtonTap(_ sender: Any) {
        let signInByEmailViewController = appAssembly.coreAssembly.signInByEmailViewController
        
//        if let navigator = self.navigationController {
//            navigator.pushViewController(signInByEmailViewController, animated: true)
//        }
        self.present(signInByEmailViewController, animated: true, completion: nil)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SignUpViewController {
    func setupUI() {
        
        signUpWithEmailBtn.isEnabled = true
    }
}
