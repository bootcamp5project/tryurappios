

import UIKit

private let reuseIdentifier = "businessCell"

class BusinessCollectionViewController: UICollectionViewController {
    
    var businessIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("BLA, BLA, ...")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

   
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return MockData.allBusiness.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BusinessCollectionViewCell
        
        
        cell.businessName.text = MockData.allBusiness[indexPath.row].businessName
        
        cell.url.text = MockData.allBusiness[indexPath.row].url
    
        return cell
    }
    
    
    
}

 // MARK: UICollectionViewDelegate

extension BusinessCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.businessIndex = indexPath.row
        performSegue(withIdentifier: "changeBusiness", sender: nil)
    }
    
}

