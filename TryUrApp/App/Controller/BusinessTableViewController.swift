//
//  BusinessTableViewController.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 20/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class BusinessTableViewController: UITableViewController {
    var responseResult: [BusinessTUA] = []
    var businessSelected: BusinessTUA?
    var aUser: UserUI?
    var deleteBusinessInteractor: DeleteBusinessInteractor = DeleteBusinessInteractorImpl()
    var showNoDataMessage: Bool = false
    var messageCanBeDisplayedInBackgroundTableView: Bool = false
    var appAssembly = AppAssembly()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        navigationItem.title = "Business"
        //navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(self.backToPreviousVC))
        
        let tView: UITableView = UITableView(frame: self.view.frame, style: .plain)
        tView.allowsSelection = false
        tableView = tView
        tableView.register(BusinessTableViewCell.self, forCellReuseIdentifier: "cellId")
        tableView.backgroundColor = UIColor.init(hexString: "595F65")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNoDataMessage = false
        messageCanBeDisplayedInBackgroundTableView = false
        navigationItem.rightBarButtonItem = nil
        
        let userEmail: String? = KeychainWrapper.standard.string(forKey: "USEREMAIL")
        let password: String? = KeychainWrapper.standard.string(forKey: "PASSWORD")
        
        aUser = UserUI(email: userEmail!, name: "", password: password!)
        let getBusinessesByUserEmailInteractor = GetBusinessesByUserEmailInteractorImpl()
        
        //spinner
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        getBusinessesByUserEmailInteractor.execute(user: aUser!, success: { (response) in
            UIViewController.removeSpinner(spinner: sv)
            
            self.messageCanBeDisplayedInBackgroundTableView = true
            
            if response.status == 200 {
                print("response.result!: \((response.result! as [BusinessTUA]).count)")
                self.responseResult = response.result!
            } else if response.status == 404 {
                self.responseResult = []
                self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addBusiness))
            }
            
            self.tableView.reloadData()
        }, onError: { (error) in
            UIViewController.removeSpinner(spinner: sv)
            self.presentError(error)
            print("error.localizedDescription: \(error.localizedDescription)")
            print("error: \(error)")
        })
    }
    
    override func didReceiveMemoryWarning() {
        super .didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("responseResult.count: \(responseResult.count)")
        if self.messageCanBeDisplayedInBackgroundTableView {
            if responseResult.count == 0 {
                let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                
                noDataLabel.text = "No data available"
                noDataLabel.textColor = UIColor.white
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
                tableView.separatorStyle = .none
            } else {
                tableView.backgroundView = nil
            }
        }
        
        return responseResult.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! BusinessTableViewCell
        
        // Configure the cell...
        myCell.myTableViewController = self
        myCell.nameLabel.text = responseResult[indexPath.row].url!
        
        return myCell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //spinner
            let sv = UIViewController.displaySpinner(onView: self.view)
            
            let aBusiness: BusinessUI = mapBusiness(from: responseResult[indexPath.row])
            deleteBusinessInteractor.execute(business: aBusiness, user: aUser!, success: { (response) in
                UIViewController.removeSpinner(spinner: sv)
                // Delete the row from the data source
                self.responseResult.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                
                if self.responseResult.count == 0 {
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addBusiness))
                }
            }, onError: { (error) in
                UIViewController.removeSpinner(spinner: sv)
                self.presentError(error)
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: - Several functions
    
    @objc func navigateToReactApp() {
        print("Show React App")
        if let url = URL(string: "tryurapp://principal/fjarilla@gmail.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @objc func addBusiness() {
        print("addBusiness Bar Button Item pressed")
        let addNewBusinessViewController = appAssembly.coreAssembly.addNewBusinessViewController
        self.present(addNewBusinessViewController, animated: true, completion: nil)
    }
    
    @objc func backToPreviousVC() {
        self.parent?.dismiss(animated: true, completion: nil)
    }
}
