
import UIKit
import RxSwift
import RxCocoa
import SwiftKeychainWrapper

public class SignInByEmailViewController: UIViewController {

    @IBOutlet weak var emailTextField: DesignableUITextField!
    @IBOutlet weak var passwordTextField: DesignableUITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    
    var disposeBag = DisposeBag()
    let authUserInteractor = AuthenticateUserInteractorImpl()
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if (AppUtils.verifyTokenExist()) {
            let user = AppUtils.getUserFromLocal()
            signIn(user: user!)
        } else {
            let email = emailTextField.rx.text.map { $0 ?? "" }
            let password = passwordTextField.rx.text.map { $0 ?? "" }
            
            let signInByEmailViewModel = SignInEmailViewModel(withUserEmail: email, password: password, didPressSignInButton: signInButton.rx.tap.asObservable())
            
            configure(with: signInByEmailViewModel)
        }
    }
    
    @IBAction func signInButtonTap(_ sender: Any) {
        
        let user = UserUI(email: emailTextField.text!, name: "", password: passwordTextField.text!)
        emailTextField.text = ""
        passwordTextField.text = ""
        signIn(user: user)
    }
    
    @IBAction func signUpButtonTap(_ sender: Any) {
        let signUpViewController = SignUpViewController()
        self.present(signUpViewController, animated: true, completion: nil)
    }    
}

extension SignInByEmailViewController {
    
    func setupUI() {
        emailTextField.setBottomBorder()
        emailTextField.clearButtonMode = .whileEditing
        passwordTextField.setBottomBorder()
        passwordTextField.isSecureTextEntry = true
        passwordTextField.clearButtonMode = .whileEditing
    }
    
    func configure(with viewModel: SignInEmailViewModel) {
        _ = viewModel.isValid.bind(to: signInButton.rx.isEnabled)
        
    }
    
    func saveOnKeyChain(userEmail: String, password: String, token: String) {
        
        _ = KeychainWrapper.standard.set(userEmail, forKey: "USEREMAIL")
        _ = KeychainWrapper.standard.set(password, forKey: "PASSWORD")
        _ = KeychainWrapper.standard.set(token, forKey: "TOKEN")
    }
    
    func presentNext() {
        // Profile
        let profileViewController = ProfileViewController()
        profileViewController.navigationItem.title = "Profile"
        let profileNavC = UINavigationController(rootViewController: profileViewController)
        profileNavC.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "user_icon"), selectedImage: nil)
        profileNavC.navigationBar.backgroundColor = UIColor(hexString: "595F65", alpha: 0)
        
        // Business
        let businessesController = BusinessTableViewController()
        let businessNavC = UINavigationController.init(rootViewController: businessesController)
        businessNavC.tabBarItem = UITabBarItem(title: "Business", image: UIImage(named: "laptop"), selectedImage: nil)
        businessNavC.navigationBar.backgroundColor = UIColor(hexString: "595F65", alpha: 0)
        
        // TabBar
        let tabBarController = UITabBarController(nibName: nil, bundle: nil)
        tabBarController.setViewControllers([businessNavC, profileNavC], animated: true)
        self.present(tabBarController, animated: true, completion: nil)
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) //This will hide the keyboard
    }
    
    func signIn(user: UserUI) {
        
        let sV = UIViewController.displaySpinner(onView: self.view)
        authUserInteractor.execute(user: user, success: { [weak self] (response) in
            UIViewController.removeSpinner(spinner: sV)
            
            if(response.status == 200) {
                self?.saveOnKeyChain(userEmail: user.email, password: user.password, token: response.resultDescription)
                self?.presentNext()
            } else {
                let error: Error = MyError.customError(message: response.resultDescription)
                self?.presentError(error)
            }
        }) { (error) in
            UIViewController.removeSpinner(spinner: sV)
            self.presentError(error)
        }
    }
}


//******
//        signInByEmailViewModel.authResponse?
//            .subscribe(onNext: { (response) in
//                print(response)
//            })
//            .disposed(by: disposeBag)








