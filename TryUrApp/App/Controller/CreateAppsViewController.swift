
import UIKit

class CreateAppsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sv = UIViewController.displaySpinnerWithoutBackground(onView: self.view)
        
        //Time out
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            UIViewController.removeSpinner(spinner: sv)
            self.performSegue(withIdentifier: "updateBusiness", sender: self)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
