//
//  ProfileViewController.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/26/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

public class ProfileViewController: UIViewController {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userCreatedTextField: UITextField!
    @IBOutlet weak var userStatusTextField: UITextField!
    
    let appAssembly = AppAssembly()
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
    }
    
    @IBAction func logoutButtonTap(_ sender: Any) {
        AppUtils.clearKeyChain()
        self.dismiss(animated: true, completion: nil)
        let signUpViewController = appAssembly.coreAssembly.signUpViewController
        self.present(signUpViewController, animated: true, completion: nil)
    }
    
    @IBAction func changePasswordButtonTap(_ sender: Any) {
    }
}

extension ProfileViewController {
    
    func setupUI(name: String, email: String, date: String, status: String) {
        userNameLabel.text = name
        userEmailTextField.text = email
        userCreatedTextField.text = date
        userStatusTextField.text = status
        
    }
    
    func getData() {
        let sV = UIViewController.displaySpinner(onView: self.view)
        let getUserInteractor = GetUserInteractorImpl()
        if let aUser = AppUtils.getUserFromLocal() {
            getUserInteractor.execute(user: aUser, success: { [weak self] (response) in
                UIViewController.removeSpinner(spinner: sV)
                let user = response.result
                let name = user?.name
                let email = user?.email
                let dateCreated = AppUtils.getDate(from: (user?.creationDate)!)
                var status = ""
                if ((user?.isActive)!) {
                    status = "Active"
                }
                self?.setupUI(name: name!, email: email!, date: dateCreated, status: status)
            }) { (error) in
                
            }
        } else {
            let error: Error = MyError.customError(message: "No user logged")
            self.presentError(error)
        }
        
        
    }
}
