//
//  PayViewController.swift
//  TryUrApp
//
//  Created by Pedro Sánchez Castro on 26/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class PayViewController: UIViewController {

    @IBAction func payOk(_ sender: Any) {
        let sv = UIViewController.displaySpinner(onView: self.view)

        //Time out
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            UIViewController.removeSpinner(spinner: sv)
            self.performSegue(withIdentifier: "updateBusiness", sender: self)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
