//
//  SignInByEmailViewModel.swift
//  TryUrApp
//
//  Created by Henry Bravo on 6/20/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct Auth {
    let token: String
}

struct AuthResponse {
    var token: String
}

struct Credentials {
    let email: String
    let password: String
}

class SignInEmailViewModel {
    
    var authResponse: Observable<Auth>?
    var emailText: Observable<String>
    var passwordText: Observable<String>
    var userInputs: Observable<Credentials>
    var isValid: Observable<Bool>
    
    init(withUserEmail email: Observable<String>, password: Observable<String>, didPressSignInButton: Observable<Void>) {
        
        emailText = email
        passwordText = password
        
        
        userInputs = Observable.combineLatest(email, password) { (email, password) in
            return Credentials(email: email, password: password)
        }
        
        isValid = userInputs.map { credentials in
            return (AppUtils.emailValidator(email: credentials.email) && credentials.password.count >= 3)
        }
    }
}

extension SignInEmailViewModel {
    
}

class MockAuthService {
    func getAuthToken(withCredentials credentials: Credentials) -> Observable<AuthResponse> {
        let dummyAuthResponse = AuthResponse(token: "dummyToken->login:\(credentials.email), password:\(credentials.password)")
        return Observable.just(dummyAuthResponse)
    }
}








//authResponse = didPressSignInButton
//    .withLatestFrom(userInputs)
//    .flatMap { [weak self] (credentials) -> Observable<AuthResponse> in
//        let user = UserUI(email: credentials.email, name: "", password: credentials.password)
//        return Observable.create { (observer) -> Disposable in
//
//            self?.authUserInteractor.execute(user: user, success: { (response) in
//                let authResp = AuthResponse(token: response.resultDescription)
//                observer.on(.next(authResp))
//                observer.on(.completed)
//
//            }, onError: { (error) in
//                print("Login in error")
//            })
//            return Disposables.create()
//        }
//
//    }
//    .map { authResponse in
//        return Auth(token: authResponse.token)
//}



// auth func ****
//    func auth(user: UserUI) -> Observable<AuthResponse> {
//        return Observable.create { (observer) -> Disposable in
//
//            self.authUserInteractor.execute(user: user, success: { (response) in
//                let authResp = AuthResponse(token: response.resultDescription)
//                observer.on(.next(authResp))
//                observer.on(.completed)
//
//            }, onError: { (error) in
//                print("Login in error")
//            })
//            return Disposables.create()
//        }
//    }







