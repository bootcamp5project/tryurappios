
import Foundation
import RxCocoa
import RxSwift

struct Model {
    let name: String
    let email: String
    let password: String
    let confirmPassword: String
}

class SignUpByEmailViewModel {
    
    var nameText: Observable<String>
    var emailText: Observable<String>
    var passwordText: Observable<String>
    var confirmPasswordText: Observable<String>
    var isValid: Observable<Bool>
    
    init(with name: Observable<String>, email: Observable<String>, password: Observable<String>, confimPassword: Observable<String>) {
        
        nameText = name
        emailText = email
        passwordText = password
        confirmPasswordText = confimPassword
        
        let userInputs = Observable.combineLatest(name, email, password, confimPassword) { name, email, password, confimPassword in
            return Model(name:name, email: email, password: password, confirmPassword: confimPassword)
        }
        
        isValid = userInputs.map {  model in
            return (model.name.count >= 3 && AppUtils.emailValidator(email: model.email) &&
                AppUtils.passwordValidator(password: model.password) && (model.password == model.confirmPassword))
        }
    }
}
