//
//  AddNewBusinessViewModel.swift
//  TryUrApp
//
//  Created by Henry Bravo on 9/24/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class AddNewBusinessViewModel {
    
    var isValid: Observable<Bool>
    var businessType: BusinessType = .BLG
    
    init(url: Observable<String>, cmsType: Observable<String>, ecommerce: ControlProperty<Bool>, blog: ControlProperty<Bool>) {
        
        let inputs = Observable.combineLatest(url, cmsType, ecommerce, blog) { (url, cmsType, ecommerce, blog) in
            return (url, cmsType, ecommerce, blog)
        }
        
        isValid = inputs.map { (arg) -> Bool in
            let (url, cmsType, ecommerce, blog) = arg
            return (url.count >= 3 && !cmsType.isEmpty && (ecommerce || blog))
        }
        
        _ = inputs.map { [weak self] (arg)  in
            let (_, _, ecommerce, blog) = arg
            
            self?.businessType = ecommerce.verify(with: blog)
        }
    }
    
}



extension Bool {
    
    func verify(with other: Bool) -> BusinessType {
        if (self == true) {
            if (other == true) {
                return .ECM_BLG
            } else {
                return .ECM
            }
        } else {
            return .BLG
        }
    }
}

enum BusinessType {
    case ECM
    case BLG
    case ECM_BLG
    
    var id: Int {
        switch self {
        case .ECM: return 0
        case .BLG: return 1
        case .ECM_BLG: return 2
        }
    }
}
// Businesstypes
// 0, Ecommerce
// 1, Blog
// 2, Ecommerce+Blog
