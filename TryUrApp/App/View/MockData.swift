
import Foundation

final class MockData {
    
    static var allBusiness : [Business] = generateBusiness()
    
    static func generateBusiness() -> [Business] {
        return [
         //   Business(businessName: "Sanchez Bicicleta"),
        //    Business(businessName: "Mara Zapatos"),
        //    Business(businessName: "Muebles Zambrano")
        ]
    }
    
    static func addBusiness(business: Business) {
        allBusiness.append(business)
    }
}
