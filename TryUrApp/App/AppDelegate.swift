//
//  AppDelegate.swift
//  TryUrApp
//
//  Created by Henry Bravo on 3/13/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let appAssembly = AppAssembly()
    var initialViewController = UIViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if AppUtils.verifyTokenExist() {
            initialViewController = appAssembly.coreAssembly.signInByEmailViewController
//            initialViewController = appAssembly.coreAssembly.profileViewController
        } else {
            initialViewController = appAssembly.coreAssembly.signUpViewController
        }
//        appAssembly.navController = UINavigationController(rootViewController: initialViewController)
//        setupUI()
        
        appAssembly.window.rootViewController = initialViewController
        appAssembly.window.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

extension AppDelegate {
    func setupUI() {
    
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = Constants.uitextfieldTextColorHex
        navigationBarAppearace.barTintColor = Constants.backgroundColorHex
    }
}






