//
//  CoreAssembly.swift
//  TryUrApp
//
//  Created by Henry Bravo on 5/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

final public class CoreAssembly{
    public private(set) lazy var signUpViewController = SignUpViewController()
    public private(set) lazy var signInByEmailViewController = SignInByEmailViewController()
    public private(set) lazy var signUpByEmailViewController = SignUpByEmailViewController()
    public private(set) lazy var addNewBusinessViewController = AddNewBusinessViewController()
    public private(set) lazy var profileViewController = ProfileViewController()
}
