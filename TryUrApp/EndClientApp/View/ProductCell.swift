//
//  ProductCell.swift
//  FastApp
//
//  Created by Fernando Jarilla on 6/3/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import Domain

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productCellImage: UIImageView!
    @IBOutlet weak var productCellTitle: UILabel!
    
    func refresh(product: Product) {
        
        let url = URL(string: (product.image!))
        self.productCellImage.kf.setImage(with: url)
        productCellTitle.text = product.title
    }
}
