//
//  PostCell.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import Domain

class PostCell: UICollectionViewCell {
    
    @IBOutlet weak var postCellTitle: UILabel!
    @IBOutlet weak var postCellContent: UITextView!
    
    func refresh(post: Post) {
        postCellTitle.text = post.title
        postCellContent.text = post.input
    }
}
