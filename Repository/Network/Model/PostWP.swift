//
//  PostWP.swift
//  Repository
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public struct PostWP: Decodable {
    public var identifier: Int64?
    public var date: String? = ""
    public var dateGtm: String? = ""
    public var guid: Rendered? = nil
    public var modified: String? = ""
    public var modifiedGtm: String? = ""
    public var slug: String? = ""
    public var status: String? = ""
    public var type: String? = ""
    public var link: String? = ""
    public var title: Rendered?
    public var content: Rendered? = nil
    public var except: Rendered? = nil
    public var featuredMedia: Int64? = 0
    public var commentStatus: String? = ""
    public var pingStatus: String? = ""
    public var template: String? = ""
    public var meta: [String]? = []
    public var links: Links? = nil

    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case date
        case dateGtm = "date_gtm"
        case guid
        case modified
        case modifiedGtm = "modified_gtm"
        case slug
        case status
        case type
        case link
        case title
        case content
        case except
        case featuredMedia = "featured_media"
        case commentStatus = "comment_status"
        case pingStatus = "ping_status"
        case template
        case meta
        case links = "_links"
    }
    
    public init(identifier: Int64, title: String) {
        self.identifier = identifier
        let titlePost = Rendered(rendered: title, protected: false)
        self.title = titlePost
    }
}

public struct Rendered: Decodable {
    public var rendered: String? = ""
    public var protected: Bool? = false
    
    private enum CodingKeys: String, CodingKey {
        case rendered
        case protected
    }
}

extension PostWP: DBPersistable {

    public typealias ManagedObject = PostObject
    
    // Enum to execute queries of Products
    public enum Query: QueryType {
        case identifier(Int)
        case all
        
        public var predicate: NSPredicate? {
            switch self {
            case .identifier(let id):
                return NSPredicate(format: "identifier == %@", id)
            case .all:
                return nil
            }
        }
        
        public var sortDescriptors: [SortDescriptor] {
            return [SortDescriptor(keyPath: "identifier", ascending: true)]
        }
    }
    
    public init(managedObejct: PostObject) {
        identifier = managedObejct.identifier
        date = ""
        dateGtm = ""
        guid = nil
        modified = ""
        modifiedGtm = ""
        slug = ""
        status = ""
        type = ""
        link = ""
        title = nil
        content = nil
        except = nil
        featuredMedia = 0
        commentStatus = ""
        pingStatus = ""
        template = ""
        meta = []
        links = nil
    }
    
    public func managedObject() -> PostObject {
        let post = PostObject()
        
        if identifier != nil {
            post.identifier = identifier ?? 0
        }
        //post.identifier = identifier!
        post.title = title!.rendered!
        post.publishedOn = date!
        post.input = content!.rendered!
        post.writer = slug!
        post.publishedIn = modified!
        if except?.rendered != nil {
            post.comments = except?.rendered ?? ""
        }
        //post.comments = except!.rendered!
        post.photo = link!
        
        return post
    }
}
