//
//  ProductWP.swift
//  FastApp
//
//  Created by Fernando Jarilla on 3/3/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift

public struct ProductWP: Decodable {
    public var identifier: Int64
    public var name: String?
    public var slug: String? = ""
    public var permalink: String? = ""
    public var dateCreated: String? = ""
    public var dateCreatedGmt: String? = ""
    public var dateModified: String? = ""
    public var dateModifiedGmt: String? = ""
    public var type: String? = ""
    public var status: String? = ""
    public var featured: Bool? = false
    public var catalogVisibility: String? = ""
    public var description: String? = ""
    public var shortDescription: String? = ""
    public var sku: String? = ""
    public var price: String? = ""
    public var regularPrice: String? = ""
    public var salePrice: String? = ""
    public var dateOnSaleFrom: String? = ""
    public var dateOnSaleFromGmt: String? = ""
    public var dateOnSaleTo: String? = ""
    public var dateOnSaleToGmt: String? = ""
    public var priceHtml: String? = ""
    public var onSale: Bool? = false
    public var purchasable: Bool? = false
    public var totalSales: Double? = 0.0
    public var virtual: Bool? = false
    public var downloadable: Bool? = false
    public var downloads: [Int64]? = []
    public var downloadLimit: Int64? = 0
    public var downloadExpiry: Int64? = 0
    public var externalUrl: String? = ""
    public var buttonText: String? = ""
    public var taxStatus: String? = ""
    public var taxClass: String? = ""
    public var manageStock: Bool? = false
    public var stockQuantity: String? = ""
    public var inStock: Bool? = false
    public var backorders: String? = ""
    public var backordersAllowed: Bool? = false
    public var backordered: Bool? = false
    public var soldIndividually: Bool? = false
    public var weight: String? = ""
    public var dimensions: Dimensions? = nil
    public var shippingRequired: Bool? = false
    public var shippingTaxable: Bool? = false
    public var shippingClass: String? = ""
    public var shippingClassId: Int64? = 0
    public var reviewsAllowed: Bool? = false
    public var averageRating: String? = ""
    public var ratingCount: Int64? = 0
    public var relatedIds: [Int64]? = []
    public var upsellIds: [Int64]? = []
    public var crossSellIds: [Int64]? = []
    public var parentId: Int64? = 0
    public var purchaseNote: String? = ""
    public var categories: [Categories]? = []
    public var tags: [String]? = []
    public var images: [Images]? = []
    public var attributes: [String]? = []
    public var defaultAttributes: [String]? = []
    public var variations: [String]? = []
    public var groupedProduts: [String]? = []
    public var menuOrder: Int64? = 0
    public var metaData: [MetaData]? = []
    public var links: Links? = nil
    
    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
        case slug
        case permalink
        case dateCreated = "date_created"
        case dateCreatedGmt = "date_created_gmt"
        case dateModified = "date_modified"
        case dateModifiedGmt = "date_modified_gmt"
        case type
        case status
        case featured
        case catalogVisibility = "catalog_visibility"
        case description
        case shortDescription = "short_description"
        case sku
        case price
        case regularPrice = "regular_price"
        case salePrice = "sale_price"
        case dateOnSaleFrom = "date_on_sale_from"
        case dateOnSaleFromGmt = "date_on_sale_from_gmt"
        case dateOnSaleTo = "date_on_sale_to"
        case dateOnSaleToGmt = "date_on_sale_to_gmt"
        case priceHtml = "price_html"
        case onSale = "on_sale"
        case purchasable
        case totalSales = "total_sales"
        case virtual
        case downloadable
        case downloads
        case downloadLimit = "download_limit"
        case downloadExpiry = "download_expiry"
        case externalUrl = "external_url"
        case buttonText = "button_text"
        case taxStatus = "tax_status"
        case taxClass = "tax_class"
        case manageStock = "manage_stock"
        case stockQuantity = "stock_quantity"
        case inStock = "in_stock"
        case backorders
        case backordersAllowed = "backorders_allowed"
        case backordered
        case soldIndividually = "sold_individually"
        case weight
        case dimensions
        case shippingRequired = "shipping_required"
        case shippingTaxable = "shipping_taxable"
        case shippingClass = "shipping_class"
        case shippingClassId = "shipping_calss_id"
        case reviewsAllowed = "reviews_allowed"
        case averageRating = "average_rating"
        case ratingCount = "rating_count"
        case relatedIds = "related_ids"
        case upsellIds = "upsell_ids"
        case crossSellIds = "cross_sell_ids"
        case parentId = "parent_id"
        case purchaseNote = "purchase_note"
        case categories
        case tags
        case images
        case attributes
        case defaultAttributes = "default_attributes"
        case variations
        case groupedProduts = "grouped_produts"
        case menuOrder = "menu_order"
        case metaData = "meta_data"
        case links = "_links"
    }
    
    public init(identifier: Int64, name: String) {
        self.identifier = identifier
        self.name = name
    }
}

public struct Dimensions: Decodable {
    public var length: String?
    public var width: String?
    public var height: String?
    
    private enum CodingKeys: String, CodingKey {
        case length
        case width
        case height
    }
}

public struct Categories: Decodable {
    public var id: Int64
    public var name: String
    public var slug: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case slug
    }
    public init(id: Int64, name: String) {
        self.id = id
        self.name = name
    }
}

public struct Images: Decodable {
    public var id: Int64 = 0
    public var dateCreated: String = ""
    public var dateCreatedGmt: String = ""
    public var dateModified: String = ""
    public var dateModifiedGmt: String = ""
    public var src: String
    public var name: String = ""
    public var alt: String = ""
    public var position: Int64 = 0
    
    private enum CodingKeys: String, CodingKey {
        case id
        case dateCreated = "date_created"
        case dateCreatedGmt = "date_created_gmt"
        case dateModified = "date_modified"
        case dateModifiedGmt = "date_modified_gmt"
        case src
        case name
        case alt
        case position
    }
    public init(src: String) {
        self.src = src
    }
}

public struct MetaData: Decodable {
    public var id: Int
    public var key: String
    public var value: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case key
        case value
    }
}

public struct Links: Decodable {
    public var `self`: [Href1]
    public var collection: [Href1]
    
    private enum CodingKeys: String, CodingKey {
        case `self`
        case collection
    }
}

public struct Href1: Decodable {
    public var href: String
    
    private enum CodingKeys: String, CodingKey {
        case href
    }
}

extension ProductWP: DBPersistable {
    public typealias ManagedObject = ProductObject
    
    // Enum to execute queries of Products
    public enum Query: QueryType {
        case identifier(Int)
        case all
        
        public var predicate: NSPredicate? {
            switch self {
                case .identifier(let id):
                    return NSPredicate(format: "identifier == %@", id)
            case .all:
                return nil
            }
        }
        
        public var sortDescriptors: [SortDescriptor] {
            return [SortDescriptor(keyPath: "identifier", ascending: true)]
        }
    }
    
    public init(managedObejct: ProductObject) {
        identifier = managedObejct.identifier
        name = managedObejct.title
        slug = ""
        permalink = ""
        dateCreated = ""
        dateCreatedGmt = ""
        dateModified = ""
        dateModifiedGmt = ""
        type = ""
        status = ""
        featured = false
        catalogVisibility = ""
        description = ""
        shortDescription = ""
        sku = ""
        price = ""
        regularPrice = ""
        salePrice = ""
        dateOnSaleFrom = ""
        dateOnSaleFromGmt = ""
        dateOnSaleTo = ""
        dateOnSaleToGmt = ""
        priceHtml = ""
        onSale = false
        purchasable = false
        totalSales = 0.0
        virtual = false
        downloadable = false
        downloads = []
        downloadLimit = 0
        downloadExpiry = 0
        externalUrl = ""
        buttonText = ""
        taxStatus = ""
        taxClass = ""
        manageStock = false
        stockQuantity = ""
        inStock = false
        backorders = ""
        backordersAllowed = false
        backordered = false
        soldIndividually = false
        weight = ""
        dimensions = nil
        shippingRequired = false
        shippingTaxable = false
        shippingClass = ""
        shippingClassId = 0
        reviewsAllowed = false
        averageRating = ""
        ratingCount = 0
        relatedIds = []
        upsellIds = []
        crossSellIds = []
        parentId = 0
        purchaseNote = ""
        categories = []
        tags = []
        images = []
        attributes = []
        defaultAttributes = []
        variations = []
        groupedProduts = []
        menuOrder = 0
        metaData  = []
        links = nil
    }
    
    public func managedObject() -> ProductObject {
        let product = ProductObject()
        
        product.identifier = identifier
        product.title = name!
        product.image = images![0].src
        if let salePrice = salePrice {
            product.salePrice = Double(salePrice) ?? 0.0
        }
        if let stockQuantity = stockQuantity {
            product.stock = Int(stockQuantity) ?? 0
        }
        product.descriptionProduct = description!
        product.shortDescription = shortDescription!
        product.length = (dimensions?.length)!
        product.width = (dimensions?.width)!
        product.height = (dimensions?.height)!
        product.taxClass = taxClass!
        
        return product
    }
}








