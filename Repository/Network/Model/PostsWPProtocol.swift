//
//  PostsWPProtocol.swift
//  Repository
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

protocol PostsWPProtocol {
    func count() -> Int
    mutating func add(post: PostWP)
    func get(index: Int) -> PostWP
}
