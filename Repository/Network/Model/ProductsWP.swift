//
//  ProductsWP.swift
//  Repository
//
//  Created by Fernando Jarilla on 25/3/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift

public struct ProductsWP: ProductsWPProtocol, Decodable {
    
    private var productsWP: [ProductWP]?
    
    public init() {
        self.productsWP = []
    }
    
    public func count() -> Int {
        return (productsWP?.count)!
    }
    
    public mutating func add(product: ProductWP) {
        productsWP?.append(product)
    }
    
    public func get(index: Int) -> ProductWP {
        return (productsWP?[index])!
    }
}
