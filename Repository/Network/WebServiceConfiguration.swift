//
//  WebServiceConfiguration.swift
//  Repository
//
//  Created by Henry Bravo on 3/20/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct WebServiceConfiguration {
    let consumerKey: String
    let consumerSecret: String
    
    private class Dummy {}
    
    public static let `default` = WebServiceConfiguration(consumerKey: Bundle(for: WebServiceConfiguration.Dummy.self).infoDictionary?["CONSUMER_KEY"] as? String ?? "",
                                                   consumerSecret: Bundle(for: WebServiceConfiguration.Dummy.self).infoDictionary?["CONSUMER_SECRET"] as? String ?? "")
    
    var parameters: [String: String] {
        return ["consumer_key": "ck_8d06b13b4f4b7fcfe4b6e3f1d323ec3ac5517f32", "consumer_secret": "cs_dc8dc47b209d2e64879eb508023ebb6273fe1382"]
    }
}
