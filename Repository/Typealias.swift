//
//  Typealias.swift
//  Repository
//
//  Created by Henry Bravo on 3/27/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift

public typealias ErrorClosure = (Error) -> Void
public typealias ProductObjectSuccessClosure = (Results<ProductObject>) -> Void
public typealias PostObjectSuccessClosure = (Results<PostObject>) -> Void
