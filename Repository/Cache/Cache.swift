//
//  Cache.swift
//  Repository
//
//  Created by Henry Bravo on 3/26/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

public protocol ReadCache {
    func getProducts(successClosure: @escaping ProductObjectSuccessClosure, errorClosure: ErrorClosure)
    //func getProduct(with id: Int64) -> ProductObject
    func getPosts(successClosure: @escaping PostObjectSuccessClosure, errorClosure: ErrorClosure)
}

public protocol WriteCache {
    func writeProducts(products: [ProductWP])
    func writePosts(posts: [PostWP])
}

public protocol Cache: ReadCache, WriteCache {}
