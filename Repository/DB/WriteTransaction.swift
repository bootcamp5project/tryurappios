//
//  WriteTransaction.swift
//  Repository
//
//  Created by Henry Bravo on 3/24/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

public final class WriteTransaction {
    public typealias ErrorClosure = (Error) -> Void
    
    private let realm: Realm
    private let disposeBag = DisposeBag()
    
    internal init(realm: Realm) {
        self.realm = realm
    }
    
    public func add<T: DBPersistable>(_ value: T, update: Bool, errorClosure: ErrorClosure?) {
        realm.add(value.managedObject(), update: update)
    }
    
    public func addAll<T: DBPersistable>(_ value: [T], update: Bool) {
        let values = value.map { $0.managedObject() }
        realm.add(values, update: update)
    }
}
