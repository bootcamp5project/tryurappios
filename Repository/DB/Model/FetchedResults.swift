//
//  FetchedResults.swift
//  Repository
//
//  Created by Henry Bravo on 3/24/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift

public final class FetchedResults<T: DBPersistable> {
    internal let results: Results<T.ManagedObject>
    
    public var count: Int {
        return results.count
    }
    
    internal init(results: Results<T.ManagedObject>) {
        self.results = results
    }
    
    public func getValue(at index: Int) -> T {
        return T(managedObejct: results[index])
    }
}
