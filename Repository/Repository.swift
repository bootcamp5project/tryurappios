//
//  Repository.swift
//  Repository
//
//  Created by Henry Bravo on 3/31/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift

public protocol Repository {
    func getAllProducts(success: @escaping ProductObjectSuccessClosure, error: ErrorClosure?)
    func getAllPosts(success: @escaping PostObjectSuccessClosure, error: ErrorClosure?)
}
