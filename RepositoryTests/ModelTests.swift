//
//  ModelTests.swift
//  RepositoryTests
//
//  Created by Fernando Jarilla on 4/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import Repository

class ModelTests: XCTestCase {
    
    var productWP: ProductWP!
    var categoria: Categories!
    var productsWP = ProductsWP()
    var postWP: PostWP!
    var content: Rendered!
    var postsWP = PostsWP()
    
    override func setUp() {
        super.setUp()
        //Creamos un ProductWP
        productWP = ProductWP(identifier: 1, name: "Producto 1")
        
        //Creamos un conjunto de productosWP
        let product2WP = ProductWP(identifier: 2, name: "Producto 2")
        let product3WP = ProductWP(identifier: 3, name: "Producto 3")
        productsWP.add(product: productWP)
        productsWP.add(product: product2WP)
        productsWP.add(product: product3WP)
        
        //Creamos una categoria
        categoria = Categories(id: 1, name: "Ropa de cama")
        
        //Creamos un PostWP
        postWP = PostWP(identifier: 1, title: "Post 1")
        
        //Creamos un conjunto de postsWP
        let post2WP = PostWP(identifier: 2, title: "Post 2")
        let post3WP = PostWP(identifier: 3, title: "Post 3")
        postsWP.add(post: postWP)
        postsWP.add(post: post2WP)
        postsWP.add(post: post3WP)
        
        //Creamos un content
        content = Rendered(rendered: "Contenido del post", protected: false)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TEST PRODUCTWP
    func testGiven_a_productWP_When_we_change_the_category_Then_the_categorie_changes() {
        productWP.categories?.append(categoria)
        XCTAssertTrue(productWP.categories![0].name == "Ropa de cama")
    }
    
    //TEST PRODUCTSWP
    func testGiven_three_productsWP_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = productsWP.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_productsWP_When_we_get_element_2_Then_the_product_name_is_Producto_2() {
        let producto = productsWP.get(index: 1)
        XCTAssertTrue(producto.name == "Producto 2")
    }
    
    //TEST POSTWP
    func testGiven_a_postWP_When_we_change_the_content_Then_the_content_changes() {
        postWP.content = content
        XCTAssertTrue(postWP.content?.rendered == "Contenido del post")
    }
    
    //TEST POSTSWP
    func testGiven_three_postsWP_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = postsWP.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_postsWP_When_we_get_element_2_Then_the_post_name_is_Post_2() {
        let post = postsWP.get(index: 1)
        XCTAssertTrue(post.title?.rendered == "Post 2")
    }
    
    
    
    
    
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
