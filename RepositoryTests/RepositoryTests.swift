//
//  RepositoryTests.swift
//  RepositoryTests
//
//  Created by Fernando Jarilla on 7/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import Repository

class RepositoryImplTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_get_all_products_works_properly() {
        let ex = expectation(description: "Expecting a getAllProducts response")
        
        RepositoryImpl().getAllProducts(success: { (success) in
                XCTAssertNotNil(success)
                ex.fulfill()
            }) { (error) in
                print("ERROR: \(error)")
                XCTAssertNil(error)
                ex.fulfill()
            }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_posts_works_properly() {
        let ex = expectation(description: "Expecting a getAllPosts response")
        
        RepositoryImpl().getAllPosts(success: { (success) in
            XCTAssertNotNil(success)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}

