//
//  WebserviceTest.swift
//  RepositoryTests
//
//  Created by Fernando Jarilla on 7/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
import RxSwift
@testable import Repository

class WebservicesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_webservice_works_properly() {
        let disposeBag = DisposeBag()
        let ex = expectation(description: "Expecting a Webservice response")
        let webService = WebService(configuration: .default)
        
        _ = webService
            .load([ProductWP].self, from: .allProducts)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (products) in
                    XCTAssertNotNil(products)
                    ex.fulfill()
            },
                onError: { (error) in
                    print("ERROR: \(error)")
                    XCTAssertNil(error)
                    ex.fulfill()
            })
            .disposed(by: disposeBag)
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}
