//
//  ProductMapper.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import Repository

public func mapProduct(from productObj: ProductObject) -> Product {
    var product = Product(title: productObj.title)
    
    product.image = productObj.image
    product.salePrice = productObj.salePrice

    product.stock = productObj.stock
    product.descriptionProduct = productObj.descriptionProduct
    product.height = productObj.height
    product.width = productObj.width
    product.length = productObj.length
    product.taxClass = productObj.taxClass
    product.shortDescription = productObj.shortDescription
    
    return product
}
