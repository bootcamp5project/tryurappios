//
//  PostMapper.swift
//  Domain
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import Repository

public func mapPost(from postObj: PostObject) -> Post {
    var post = Post(title: postObj.title)

    post.identifier = postObj.identifier
    post.publishedOn = postObj.publishedOn
    post.input = postObj.input
    post.writer = postObj.writer
    post.publishedIn = postObj.publishedIn
    post.comments = postObj.comments
    post.photo = postObj.photo
    
    return post
}
