//
//  AgregateProtocol.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol Agregate {
    associatedtype AgregateType
    
    func count() -> Int
    func getAll() -> [AgregateType]
    func get(index: Int) -> AgregateType
    
    mutating func add(item: AgregateType)
    
}
