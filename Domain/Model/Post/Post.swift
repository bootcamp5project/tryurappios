//
//  Post.swift
//  Domain
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct Post {
    public var identifier: Int64 = 0
    public var title: String = ""
    public var publishedOn: String = ""
    public var input: String = ""
    public var writer: String = ""
    public var publishedIn: String = ""
    public var comments: String = ""
    public var photo: String = ""
    
    public init(title: String) {
        self.title = title
    }
}
