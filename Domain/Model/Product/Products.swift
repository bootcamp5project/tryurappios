//
//  Products.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct Products: Agregate {
    public typealias AgregateType = Product
    
    private var _products: [Product]?
    
    public init() {
        self._products = []
    }
    
    public func count() -> Int {
        return (_products?.count)!
    }
    
    public func getAll() -> [Product] {
        return _products!
    }
    
    public func get(index: Int) -> Product {
        return _products![index]
    }
    
    public mutating func add(item: Product) {
        _products?.append(item)
    }
}
