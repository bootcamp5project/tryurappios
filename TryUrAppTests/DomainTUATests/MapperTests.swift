//
//  MapperTests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class MapperTests: XCTestCase {
    
    var userUI: UserUI!
    var userTUA: UserTUA!
    
    var businessUI: BusinessUI!
    var businessTUA: BusinessTUA!
    
    var responseApiUI: ResponseApiUI!
    var responseTUA: ResponseTUA!
    
    var responseBusinessUI: ResponseBusinessUI!
    var responseBusinessTUA: ResponseBusinessTUA!
    
    override func setUp() {
        super.setUp()
        
        userUI = UserUI(email: "userUI@gmail.com", name: "UsuarioUI", password: "Jarilla1")
        userTUA = UserTUA(email: "userTUA@gmail.com")
        userTUA.name = "UsuarioTUA"
        userTUA.password = "Jarilla1"
        
        businessUI = BusinessUI(url: "www.businessUI.com", userEmail: "businessUI@gmail.com", idBusinessType: 1, idCmsType: 1)
        businessTUA = BusinessTUA(url: "www.businessTUA.com", userEmail: "businessTUA@gmail.com", idBusinessType: 1, idCmsType: 1)
        
        responseApiUI = ResponseApiUI(status: 200)
        responseTUA = ResponseTUA(status: 200, resultDescription: "Operación correcta", result: nil)
        
        responseBusinessUI = ResponseBusinessUI(status: 200)
        responseBusinessTUA = ResponseBusinessTUA(status: 200, resultDescription: "Operación OK", result: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TEST USER
    func testGiven_a_userUI_When_we_map_the_userUI_Then_the_userTUA_exits() {
        let user = mapUser(from: userUI)
        XCTAssertTrue(user.email == "userUI@gmail.com")
        XCTAssertTrue(user.name == "UsuarioUI")
        XCTAssertTrue(user.creationDate == "")
    }
    
    func testGiven_a_userTUA_When_we_map_the_userTUA_Then_the_userUI_exits() {
        let user = mapUser(from: userTUA)
        XCTAssertTrue(user.email == "userTUA@gmail.com")
        XCTAssertTrue(user.name == "UsuarioTUA")
        XCTAssertTrue(user.token == "")
    }
    
    //TEST BUSINESS
    func testGiven_a_businessUI_When_we_map_the_businessUI_Then_the_businessTUA_exits() {
        let business = mapBusiness(from: businessUI)
        XCTAssertTrue(business.url == "www.businessUI.com")
        XCTAssertTrue(business.userEmail == "businessUI@gmail.com")
        XCTAssertTrue(business.logoDataName == "")
    }
    
    func testGiven_a_businessTUA_When_we_map_the_businessTUA_Then_the_businessUI_exits() {
        let business = mapBusiness(from: businessTUA)
        XCTAssertTrue(business.url == "www.businessTUA.com")
        XCTAssertTrue(business.userEmail == "businessTUA@gmail.com")
        XCTAssertTrue(business.logoDataName == "")
    }
    
    //TEST RESPONSE API
    func testGiven_a_responseApiUI_When_we_map_the_responseApiUI_Then_the_responseTUA_exits() {
        let response = mapResponse(from: responseApiUI)
        XCTAssertTrue(response.status == 200)
        XCTAssertTrue(response.result == nil)
    }
    
    func testGiven_a_responseTUA_When_we_map_the_responseTUA_Then_the_responseApiUI_exits() {
        let response = mapResponse(from: responseTUA)
        XCTAssertTrue(response.status == 200)
        XCTAssertTrue(response.result == nil)
    }
    
    //TEST RESPONSE BUSINESS
}
