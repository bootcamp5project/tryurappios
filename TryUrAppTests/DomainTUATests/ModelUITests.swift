//
//  ModelUITests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class ModelUITests: XCTestCase {
   
    var userUI1: UserUI!
    var userUI2: UserUI!
    var userUI3: UserUI!
    var usersUI: UsersUI!
    var businessUI1: BusinessUI!
    var businessUI2: BusinessUI!
    var businessUI3: BusinessUI!
    var businessesUI: BusinessesUI!
    var responseApiUI: ResponseApiUI!
    var responseBusinessUI: ResponseBusinessUI!
    
    override func setUp() {
        super.setUp()
        
        userUI1 = UserUI(email: "userUI1@gmail.com", name: "User 1", password: "Jarilla1")
        userUI2 = UserUI(email: "userUI2@gmail.com", name: "User 2", password: "Jarilla1")
        userUI3 = UserUI(email: "userUI3@gmail.com", name: "User 3", password: "Jarilla1")
        usersUI = UsersUI()
        usersUI.add(item: userUI1)
        usersUI.add(item: userUI2)
        usersUI.add(item: userUI3)
        
        businessUI1 = BusinessUI(url: "www.businessUI1.com", userEmail: "businessUI1@gmail.com", idBusinessType: 1, idCmsType: 1)
        businessUI2 = BusinessUI(url: "www.businessUI2.com", userEmail: "businessUI2@gmail.com", idBusinessType: 1, idCmsType: 1)
        businessUI3 = BusinessUI(url: "www.businessUI2.com", userEmail: "businessUI2@gmail.com", idBusinessType: 1, idCmsType: 1)
        businessesUI = BusinessesUI()
        businessesUI.add(item: businessUI1)
        businessesUI.add(item: businessUI2)
        businessesUI.add(item: businessUI3)
        
        responseApiUI = ResponseApiUI(status: 200)
        responseBusinessUI = ResponseBusinessUI(status: 200)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TESTS USERS
    func testGiven_a_userUI_When_we_change_the_token_Then_the_token_changes() {
        userUI1.token = "Este es el nuevo token"
        XCTAssertTrue(userUI1.token == "Este es el nuevo token")
    }
    
    func testGiven_three_usersUI_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = usersUI.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_usersUI_When_we_get_element_2_Then_the_userTUA_name_is_User_2() {
        let user = usersUI.get(index: 1)
        XCTAssertTrue(user.name == "User 2")
    }
    
    
    //TESTS BUSINESS
    func testGiven_a_businessUI_When_we_change_the_logoDataName_Then_the_logoDataName_changes() {
        businessUI1.logoDataName = "logoDataTest"
        XCTAssertTrue(businessUI1.logoDataName == "logoDataTest")
    }
    
    func testGiven_three_businessesUI_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = businessesUI.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_businessesUI_When_we_get_element_2_Then_the_businessUI_name_is_User_2() {
        let user = businessesUI.get(index: 1)
        XCTAssertTrue(user.userEmail == "businessUI2@gmail.com")
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //TEST RESPONSE API
    func testGiven_a_responseApiUI_When_we_change_the_result_Then_the_result_changes() {
        responseApiUI.result = userUI1
        XCTAssertTrue(responseApiUI.result?.name == "User 1")
    }
    
    //TEST RESPONSE BUSINESS
    func testGiven_a_responseBusinessUI_When_we_change_the_result_Then_the_result_changes() {
        responseBusinessUI.result = businessUI1
        XCTAssertTrue(responseBusinessUI.result?.url == "www.businessUI1.com")
    }
    
}
