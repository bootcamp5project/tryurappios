//
//  InteractorsTests.swift
//  DomainTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import Domain

class InteractorsImplTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_get_all_products_works_properly() {
        let ex = expectation(description: "Expecting a getAllProducts response")
        
        let getAllProductsInteractor = GetAllProductsInteractorImpl()
        
        getAllProductsInteractor.execute(success: { (products) in
            XCTAssertNotNil(products)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_posts_works_properly() {
        let ex = expectation(description: "Expecting a getAllPosts response")
        
        let getAllPostsInteractor = GetAllPostsInteractorImpl()
        
        getAllPostsInteractor.execute(success: { (posts) in
            XCTAssertNotNil(posts)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}
