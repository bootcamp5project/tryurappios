//
//  MapperTUATests.swift
//  DomainTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
import Repository
@testable import Domain

class MapperTUATests: XCTestCase {
    
    var productObject1: ProductObject!
    var postObject1: PostObject!
    
    override func setUp() {
        super.setUp()
        
        //Crear un ProductObject
        productObject1 = ProductObject(title: "Producto 1")
        productObject1.descriptionProduct = "Este es el producto 1"
        productObject1.height = "1,23 m"
        productObject1.length = "2,23 m"
        productObject1.width = "41 cm"
        productObject1.identifier = 1
        productObject1.salePrice = 23.5
        productObject1.stock = 5
        productObject1.taxClass = "21%"
        
        //Crear un PostObject
        postObject1 = PostObject(title: "Post 1")
        postObject1.identifier = 1
        postObject1.comments = "Comentario al post 1"
        postObject1.input = "Este es el post 1"
        postObject1.publishedIn = "11-05-2018"
        postObject1.publishedOn = "12-05-2018"
        postObject1.writer = "Fernando"
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TEST PRODUCT
    func testGiven_a_productObject_When_we_map_the_product_Then_the_product_exits() {
        let product = mapProduct(from: productObject1)
        XCTAssertTrue(product.salePrice == 23.5)
        XCTAssertTrue(product.title == "Producto 1")
        XCTAssertTrue(product.image == "")
    }
    
    //TEST POST
    func testGiven_a_postObject_When_we_map_the_post_Then_the_post_exits() {
        let post = mapPost(from: postObject1)
        XCTAssertTrue(post.identifier == 1)
        XCTAssertTrue(post.input == "Este es el post 1")
        XCTAssertTrue(post.photo == "")
    }
}
