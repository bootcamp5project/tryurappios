//
//  UserTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct UserUI {
    public var email: String = ""
    public var name: String = ""
    public var password: String = ""
    public var token: String = ""
    
    public init(email: String) {
        self.email = email
    }
    
    public init(email: String, name: String, password: String) {
        self.email = email
        self.name = name
        self.password = password
    }
}

extension UserUI {
    public func toString() -> String {
        return "\(email) - \(name) - \(password)"
    }
}
