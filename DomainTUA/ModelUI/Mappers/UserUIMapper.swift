//
//  UserUIMapper.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public func mapUser(from userUI: UserUI) -> UserTUA {
    var user = UserTUA(email: userUI.email)
    
    user.name = userUI.name
    user.password = userUI.password
    
    return user
}

public func mapUser(from userTUA: UserTUA) -> UserUI {
    var user = UserUI(email: userTUA.email)
    
    user.name = userTUA.name!
    user.password = userTUA.password!
    
    return user
}
