//
//  BusinessUIMapper.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public func mapBusiness(from businessUI: BusinessUI) -> BusinessTUA {
    var business = BusinessTUA(url: businessUI.url,
                              userEmail: businessUI.userEmail,
                              idBusinessType: businessUI.idBusinessType,
                              idCmsType: businessUI.idCmsType)
    //business.token = businessUI.token
    business.url = businessUI.url
    business.idLoadingType = businessUI.idLoadingType
    business.idAppIcon = businessUI.idAppIcon
    business.firstColorIdentifier = businessUI.firstColorIdentifier
    business.secondColorIdentifier = businessUI.secondColorIdentifier
    business.thirdColorIdentifier = businessUI.thirdColorIdentifier
    business.idFont = businessUI.idFont
    business.isValidInfo = businessUI.isValidInfo
    business.logoDataName = businessUI.logoDataName
    business.welcomeImageDataName = businessUI.welcomeImageDataName
    business.buildApp = businessUI.buildApp
    business.businessName = businessUI.businessName
    
    return business
}

public func mapBusiness(from businessTUA: BusinessTUA) -> BusinessUI {
    var business = BusinessUI(url: businessTUA.url!,
                              userEmail: businessTUA.userEmail!,
                              idBusinessType: businessTUA.idBusinessType!,
                              idCmsType: businessTUA.idCmsType!)
    //business.token = businessTUA.token!
    business.url = businessTUA.url!
    business.idLoadingType = businessTUA.idLoadingType!
    business.idAppIcon = businessTUA.idAppIcon!
    business.firstColorIdentifier = businessTUA.firstColorIdentifier!
    business.secondColorIdentifier = businessTUA.secondColorIdentifier!
    business.thirdColorIdentifier = businessTUA.thirdColorIdentifier!
    business.idFont = businessTUA.idFont!
    business.isValidInfo = businessTUA.isValidInfo!
    business.logoDataName = businessTUA.logoDataName!
    business.welcomeImageDataName = businessTUA.welcomeImageDataName!
    business.buildApp = businessTUA.buildApp!
    // business.businessName = businessTUA.businessName!
    
    if let businessName = businessTUA.businessName {
        business.businessName = businessName
    }
    
    return business
}

