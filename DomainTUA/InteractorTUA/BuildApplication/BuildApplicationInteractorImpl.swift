//
//  BuildApplicationInteractorImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 24/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class BuildApplicationInteractorImpl: SaveBusinessSettingsInteractor {
    
    public typealias ItemsType = BusinessUI
    
    public init() {}
    
    public func execute(business: BusinessUI, user: UserUI, success: @escaping (ResponseBusinessTUA) -> Void, onError: @escaping ErrorClosure) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let businessTUA = mapBusiness(from: business)
        let userTUA = mapUser(from: user)
        repository.saveBusinessSettings(business: businessTUA, user: userTUA, success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
