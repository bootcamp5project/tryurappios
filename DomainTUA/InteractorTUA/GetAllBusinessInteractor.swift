//
//  GetAllBusinessInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol GetAllBusinessInteractor {
    func execute(success: @escaping ResponseBusinessesSuccessClosure, onError: @escaping ErrorClosure)
}
