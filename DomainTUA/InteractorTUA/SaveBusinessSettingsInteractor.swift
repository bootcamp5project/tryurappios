//
//  SaveBusinessSettingsInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol SaveBusinessSettingsInteractor {
    func execute(business: BusinessUI, user: UserUI, success: @escaping ResponseBusinessSuccessClosure, onError: @escaping ErrorClosure)
}
