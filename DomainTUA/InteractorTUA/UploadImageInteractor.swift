//
//  UploadImageInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol UploadImageInteractor {
    func execute(business: BusinessUI, user: UserUI, imageType: String, image: String, success: @escaping ResponseBusinessSuccessClosure, onError: @escaping ErrorClosure)
}

