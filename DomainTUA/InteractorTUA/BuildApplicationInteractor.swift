//
//  BuildApplicationInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 24/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol BuildApplicationInteractor {
    func execute(business: BusinessUI, user: UserUI, success: @escaping ResponseBusinessSuccessClosure, onError: @escaping ErrorClosure)
}
