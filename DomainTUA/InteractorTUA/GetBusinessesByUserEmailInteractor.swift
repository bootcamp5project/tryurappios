//
//  GetBusinessesByUserEmailInteractor.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 11/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol GetBusinessesByUserEmailInteractor {
    func execute(user: UserUI, success: @escaping ResponseBusinessesSuccessClosure, onError: @escaping ErrorClosure)
}
